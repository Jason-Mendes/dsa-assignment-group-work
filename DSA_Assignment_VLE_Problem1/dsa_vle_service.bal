import  ballerina/http;
import ballerina/io;

LearnerProfile [] LPList = [];
LearnerProfiles LP = LPList;
LearnerCourse [] LCList = [];
LearnerCourses LC = LCList;

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

 service  /dsalearnerprofiles  on  ep0  {
        resource  function  post  LearnerProfiles(@http:Payload  {} LearnerProfile  payload)  returns  http:Created {
        io:println("Add New Learner Profile...");
        LP.push(payload);
        http:Created PostCreated = {body: "Successfully added to the Learner Profile LPList"};
        return PostCreated;
    }
        resource  function  get  LearnerProfiles()  returns  LearnerProfile[] {
            io:println("Display Student Profiles list to the client");
            return LPList;
    }
        resource  function  put  LearnerProfiles/[int  LearnerProfile_UserName](@http:Payload  {} LearnerProfile  payload)  returns  http:Ok {
            http:Ok PutOk = {body: "Learner Profile Successfully updated"};
            return PutOk;
    }
        resource  function  get  LearnerProfiles/[int  LearnerProfile_UserName]()  returns  LearnerProfile {
            io:println("Display Single Student Profile to the client");
            return LP[LearnerProfile_UserName];
    }
        resource  function  post  LearnerCourse(@http:Payload  {} LearnerCourse  payload)  returns  http:Created {
            io:println("Add New Learner Course...");
            LCList.push(payload);
            http:Created PostCreated = {body: "Successfully added to the Learner Course LCList"};
            return PostCreated;
    }
        resource  function  get  LearnerCourse()  returns  LearnerCourse[] {
            io:println("Display Student Course list to the client");
            return LCList;
    }
        resource  function  put  LearnerCourse/[int  Course_UserName](@http:Payload  {} LearnerCourse  payload)  returns  http:Ok {
            http:Ok PutOk = {body: "Learner Course Successfully updated"};
            return PutOk;
    }
        resource  function  get  LearnerCourse/[int  Course_UserName]()  returns  LearnerCourse {
            io:println("Display Single Student Course to the client");
            return LC[Course_UserName];
    }
}
