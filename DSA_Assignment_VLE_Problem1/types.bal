public type AudioContents record {
    string audioFileName?;
    string audioFileDescription?;
    string audioDifficulty?;
};

public type LearnerProfiles LearnerProfile[];

public type LearnerCourses LearnerCourse[];

public type VideoContents record {
    string videoFileName?;
};

public type LearnerProfile record {
    string UserName?;
    string LName?;
    string FName?;
    PreferredFormats[] preferred_formats?;
    PastSubjects[] past_subjects?;
};

public type PreferredFormats record {
    string audio?;
    string video?;
    string text?;
};

public type LearnerCourse record {
    string CourseName?;
};

public type LearningMaterials record {
    LearnerCourse[] Learner_Course?;
    record  { record  { AudioContents[] audio?; TextContents[] text?;}  required?; record  { VideoContents[] video?; AudioContents[] audio?;}  suggested?;}  Learning_objects?;
};

public type TextContents record {
    string textcontent?;
};

public type PastSubjects record {
    string LearnerCourse?;
    string score?;
};
