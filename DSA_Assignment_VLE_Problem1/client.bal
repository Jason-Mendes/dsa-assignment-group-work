import ballerina/http;
import ballerina/io;

public function main() returns error?{

    Client clientEp = check new();

    io:println("Virtual Learning Environment Main Screen");
    boolean choice = true;

    while (choice == true){
        io:println("********************************************************");
        io:println("Please read the tasks from the options Below");
        io:println("********************************************************");
        io:println("1. Add a Learner Profile");
        io:println("2. Get list of all Learners Profiles");
        io:println("3. Get a Single Learner Profile");
        io:println("4. Add a Learner Course");
        io:println("5. Get list of all Learners Profiles");
        io:println("6. Get a single Learner Course");
        io:println("7. End Virtual Learning Environment");
        io:println("********************************************************");
        string SelectedTask = io:readln("Please type the number of a task from the options Above: ");

    match SelectedTask{
        "1" => {
            LearnerProfile [] LPList = [];
            string UName = io:readln("Enter Your User Name: ");
            string|error UserNameInserted = UName;
            string InsertedUName = check UserNameInserted;
            string LName = io:readln("Enter Your Last Name: ");
            string FName = io:readln("Enter Your First Name: ");
            boolean input = true;

            json[] PFArray = [];
            io:println("********************************************************");
            io:println("Preffered Formats");
            io:println("********************************************************");
            while(input == true) {
                string PFPayload = io:readln("Enter your Preferred format: ");
                PFArray.push(PFPayload);

                string ContinueDecision = io:readln("Would you like to Add another preferred format? Y/N: ");

                if (ContinueDecision == "Y"){
                    input = true;
                }
                else if(ContinueDecision == "N"){
                    input = false;
                }
                else {
                    io:println("Invalid Choice");
                    input = false;
                }
            }
            input = true;
            
            json[] PSArray = [];
            io:println("Enter Your Past subjects");
            while(input == true){
                string CourseName = io:readln("enter course name: ");
                string ScoreMark = io:readln("Enter Score: ");

                json CSPayload = {Course: CourseName , Score: ScoreMark};
                string ContinueDecision = io:readln("Would you like to Add another Course and Score? Y/N: ");
                PSArray.push(CSPayload);

                if(ContinueDecision == "Y"){
                    input = true;
                }
                else if(ContinueDecision == "N"){
                    input = false;
                }
                else {
                    io:println("Invalid Choice");
                    input = false;
                }
                }
                LearnerProfile LPObject = {
                    UserName: check UserNameInserted,
                    LastName: LName,
                    FirstName: FName,
                    PrefferedFormats: PFArray,
                    PastSubjects: PSArray
                    };
                    
                error? LearnerProfile = clientEp ->addLearnerProfiles(LPObject);
                io:println(LearnerProfile);
                }
                
                "2" => {
                    LearnerProfiles|error LearnerProfile = check clientEp ->getLearnerProfiles();
                    io:println(LearnerProfile);
                }
            }
        }
    }
}       

# This is an API that allows for communication between a client and a service by creating and updating learner profiles and more.
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://dsalearnerprofiles.com") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    # Add LearnerProfiles
    #
    # + payload - This is the LearnerProfiles object
    # + return - You have Created a Learner Profile.
    remote isolated function addLearnerProfiles(LearnerProfile payload) returns error? {
        string  path = string `/LearnerProfiles`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    # Get LearnerProfiles
    #
    # + return - Successfull List of Learner Profiles displayed.
    remote isolated function getLearnerProfiles() returns LearnerProfiles|error {
        string  path = string `/LearnerProfiles`;
        LearnerProfiles response = check self.clientEp-> get(path, targetType = LearnerProfiles);
        return response;
    }
    # Updating LearnerProfiles
    #
    # + LearnerProfile_UserName - Using the Learners Username
    # + payload - This is the LearnerProfile object
    # + return - You have Updated a Learner Profile.
    remote isolated function updateLearnerProfile(string LearnerProfile_UserName, LearnerProfile payload) returns error? {
        string  path = string `/LearnerProfiles/${LearnerProfile_UserName}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> put(path, request, targetType=http:Response);
    }
    # Get LearnerProfile
    #
    # + LearnerProfile_UserName - Using the Learners Username to get a Learner Profile
    # + return - Successfull Learner Profile returned.
    remote isolated function getLearnerProfile(string LearnerProfile_UserName) returns LearnerProfile|error {
        string  path = string `/LearnerProfiles/${LearnerProfile_UserName}`;
        LearnerProfile response = check self.clientEp-> get(path, targetType = LearnerProfile);
        return response;
    }
    # Add LearnerCourses
    #
    # + payload - This is the LearnerCourse object
    # + return - You have Created a Learner Course.
    remote isolated function addLearnerCourse(LearnerCourse payload) returns error? {
        string  path = string `/LearnerCourse`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    # Get Learner Courses
    #
    # + return - Successfull List of Learner Courses displayed.
    remote isolated function getLearnerCourse() returns LearnerCourses|error {
        string  path = string `/LearnerCourse`;
        LearnerCourses response = check self.clientEp-> get(path, targetType = LearnerCourses);
        return response;
    }
    # Updating Learner Course
    #
    # + Course_UserName - Using the Learners Username
    # + payload - This is the LearnerCourse object
    # + return - You have Updated a Learner Course.
    remote isolated function updateLearnerCourse(string Course_UserName, LearnerCourse payload) returns error? {
        string  path = string `/LearnerCourse/${Course_UserName}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> put(path, request, targetType=http:Response);
    }
    # Get Learner_Course
    #
    # + Course_UserName - Using the Learners Username to get a learner Course
    # + return - Successfull Learner Course returned.
    remote isolated function getlearnerCourse(string Course_UserName) returns LearnerCourse|error {
        string  path = string `/LearnerCourse/${Course_UserName}`;
        LearnerCourse response = check self.clientEp-> get(path, targetType = LearnerCourse);
        return response;
    }
}
