import ballerina/grpc;

public isolated client class gRPCServiceAllFunctionsClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(AddRequestFunc|ContextAddRequestFunc req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        AddRequestFunc message;
        if (req is ContextAddRequestFunc) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("gRPCServiceAllFunctions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function add_new_fnContext(AddRequestFunc|ContextAddRequestFunc req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        AddRequestFunc message;
        if (req is ContextAddRequestFunc) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("gRPCServiceAllFunctions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function delete_fn(DeleteFuncRequest|ContextDeleteFuncRequest req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        DeleteFuncRequest message;
        if (req is ContextDeleteFuncRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("gRPCServiceAllFunctions/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function delete_fnContext(DeleteFuncRequest|ContextDeleteFuncRequest req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        DeleteFuncRequest message;
        if (req is ContextDeleteFuncRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("gRPCServiceAllFunctions/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function show_fn(ShowFuncRequest|ContextShowFuncRequest req) returns (FuncVerResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFuncRequest message;
        if (req is ContextShowFuncRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("gRPCServiceAllFunctions/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <FuncVerResponse>result;
    }

    isolated remote function show_fnContext(ShowFuncRequest|ContextShowFuncRequest req) returns (ContextFuncVerResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFuncRequest message;
        if (req is ContextShowFuncRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("gRPCServiceAllFunctions/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <FuncVerResponse>result, headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("gRPCServiceAllFunctions/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns(ShowAllFuncRequest|ContextShowAllFuncRequest req) returns stream<AddRequestFunc, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        ShowAllFuncRequest message;
        if (req is ContextShowAllFuncRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("gRPCServiceAllFunctions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        AddRequestFuncStream outputStream = new AddRequestFuncStream(result);
        return new stream<AddRequestFunc, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(ShowAllFuncRequest|ContextShowAllFuncRequest req) returns ContextAddRequestFuncStream|grpc:Error {
        map<string|string[]> headers = {};
        ShowAllFuncRequest message;
        if (req is ContextShowAllFuncRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("gRPCServiceAllFunctions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        AddRequestFuncStream outputStream = new AddRequestFuncStream(result);
        return {content: new stream<AddRequestFunc, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("gRPCServiceAllFunctions/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAddRequestStreamFunc(AddRequestStreamFunc message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAddRequestStreamFunc(ContextAddRequestStreamFunc message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class AddRequestFuncStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|AddRequestFunc value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|AddRequestFunc value;|} nextRecord = {value: <AddRequestFunc>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendShowAllFuncCriteriaRequest(ShowAllFuncCriteriaRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextShowAllFuncCriteriaRequest(ContextShowAllFuncCriteriaRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveAddRequestFunc() returns AddRequestFunc|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <AddRequestFunc>payload;
        }
    }

    isolated remote function receiveContextAddRequestFunc() returns ContextAddRequestFunc|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <AddRequestFunc>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class GRPCServiceAllFunctionsFuncVerResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFuncVerResponse(FuncVerResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFuncVerResponse(ContextFuncVerResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class GRPCServiceAllFunctionsAddRequestFuncCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAddRequestFunc(AddRequestFunc response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAddRequestFunc(ContextAddRequestFunc response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class GRPCServiceAllFunctionsStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextAddRequestFuncStream record {|
    stream<AddRequestFunc, error?> content;
    map<string|string[]> headers;
|};

public type ContextAddRequestStreamFuncStream record {|
    stream<AddRequestStreamFunc, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowAllFuncCriteriaRequestStream record {|
    stream<ShowAllFuncCriteriaRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowFuncRequest record {|
    ShowFuncRequest content;
    map<string|string[]> headers;
|};

public type ContextAddRequestFunc record {|
    AddRequestFunc content;
    map<string|string[]> headers;
|};

public type ContextAddRequestStreamFunc record {|
    AddRequestStreamFunc content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextFuncVerResponse record {|
    FuncVerResponse content;
    map<string|string[]> headers;
|};

public type ContextShowAllFuncRequest record {|
    ShowAllFuncRequest content;
    map<string|string[]> headers;
|};

public type ContextShowAllFuncCriteriaRequest record {|
    ShowAllFuncCriteriaRequest content;
    map<string|string[]> headers;
|};

public type ContextDeleteFuncRequest record {|
    DeleteFuncRequest content;
    map<string|string[]> headers;
|};

public type ShowFuncRequest record {|
    string FuncId = "";
|};

public type AddRequestFunc record {|
    string FuncId = "";
    CreateDevMetadataRequestFunc DevMetadata = {};
    FuncVerResponse functionVersion = {};
    string contents = "";
|};

public type AddRequestStreamFunc record {|
    string Id = "";
    string name = "";
    FuncVerResponse[] versions = [];
|};

public type CreateDevMetadataRequestFunc record {|
    string FullName = "";
    string EmailAddress = "";
    string language = "";
    string FunctionalityKeywords = "";
|};

public type FuncVerResponse record {|
    int FuncVer = 0;
|};

public type ShowAllFuncRequest record {|
    string FuncName = "";
|};

public type GenDevMetadataResponseFunc record {|
    string UserName = "";
|};

public type ShowAllFuncCriteriaRequest record {|
    string AllFuncCriteria = "";
|};

public type DeleteFuncRequest record {|
    string DeleteF = "";
|};

const string ROOT_DESCRIPTOR = "0A1050726F746F635F496D702E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22B0010A1C4372656174654465764D657461646174615265717565737446756E63121A0A0846756C6C4E616D65180120012809520846756C6C4E616D6512220A0C456D61696C41646472657373180220012809520C456D61696C41646472657373121A0A086C616E677561676518032001280952086C616E677561676512340A1546756E6374696F6E616C6974794B6579776F726473180420012809521546756E6374696F6E616C6974794B6579776F72647322380A1A47656E4465764D65746164617461526573706F6E736546756E63121A0A08557365724E616D651801200128095208557365724E616D6522C1010A0E4164645265717565737446756E6312160A0646756E634964180120012809520646756E634964123F0A0B4465764D6574616461746118022001280B321D2E4372656174654465764D657461646174615265717565737446756E63520B4465764D65746164617461123A0A0F66756E6374696F6E56657273696F6E18032001280B32102E46756E63566572526573706F6E7365520F66756E6374696F6E56657273696F6E121A0A08636F6E74656E74731804200128095208636F6E74656E747322680A144164645265717565737453747265616D46756E63120E0A0249641801200128095202496412120A046E616D6518022001280952046E616D65122C0A0876657273696F6E7318032003280B32102E46756E63566572526573706F6E7365520876657273696F6E73222D0A1144656C65746546756E635265717565737412180A0744656C65746546180120012809520744656C6574654622290A0F53686F7746756E635265717565737412160A0646756E634964180120012809520646756E634964222B0A0F46756E63566572526573706F6E736512180A0746756E63566572180120012803520746756E6356657222300A1253686F77416C6C46756E6352657175657374121A0A0846756E634E616D65180120012809520846756E634E616D6522460A1A53686F77416C6C46756E6343726974657269615265717565737412280A0F416C6C46756E634372697465726961180120012809520F416C6C46756E6343726974657269613296030A176752504353657276696365416C6C46756E6374696F6E73123D0A0A6164645F6E65775F666E120F2E4164645265717565737446756E631A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565220012420A076164645F666E7312152E4164645265717565737453747265616D46756E631A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756522002801123F0A0964656C6574655F666E12122E44656C65746546756E63526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652200122F0A0773686F775F666E12102E53686F7746756E63526571756573741A102E46756E63566572526573706F6E7365220012380A0C73686F775F616C6C5F666E7312132E53686F77416C6C46756E63526571756573741A0F2E4164645265717565737446756E6322003001124C0A1673686F775F616C6C5F776974685F6372697465726961121B2E53686F77416C6C46756E634372697465726961526571756573741A0F2E4164645265717565737446756E63220028013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"Protoc_Imp.proto": "0A1050726F746F635F496D702E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22B0010A1C4372656174654465764D657461646174615265717565737446756E63121A0A0846756C6C4E616D65180120012809520846756C6C4E616D6512220A0C456D61696C41646472657373180220012809520C456D61696C41646472657373121A0A086C616E677561676518032001280952086C616E677561676512340A1546756E6374696F6E616C6974794B6579776F726473180420012809521546756E6374696F6E616C6974794B6579776F72647322380A1A47656E4465764D65746164617461526573706F6E736546756E63121A0A08557365724E616D651801200128095208557365724E616D6522C1010A0E4164645265717565737446756E6312160A0646756E634964180120012809520646756E634964123F0A0B4465764D6574616461746118022001280B321D2E4372656174654465764D657461646174615265717565737446756E63520B4465764D65746164617461123A0A0F66756E6374696F6E56657273696F6E18032001280B32102E46756E63566572526573706F6E7365520F66756E6374696F6E56657273696F6E121A0A08636F6E74656E74731804200128095208636F6E74656E747322680A144164645265717565737453747265616D46756E63120E0A0249641801200128095202496412120A046E616D6518022001280952046E616D65122C0A0876657273696F6E7318032003280B32102E46756E63566572526573706F6E7365520876657273696F6E73222D0A1144656C65746546756E635265717565737412180A0744656C65746546180120012809520744656C6574654622290A0F53686F7746756E635265717565737412160A0646756E634964180120012809520646756E634964222B0A0F46756E63566572526573706F6E736512180A0746756E63566572180120012803520746756E6356657222300A1253686F77416C6C46756E6352657175657374121A0A0846756E634E616D65180120012809520846756E634E616D6522460A1A53686F77416C6C46756E6343726974657269615265717565737412280A0F416C6C46756E634372697465726961180120012809520F416C6C46756E6343726974657269613296030A176752504353657276696365416C6C46756E6374696F6E73123D0A0A6164645F6E65775F666E120F2E4164645265717565737446756E631A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565220012420A076164645F666E7312152E4164645265717565737453747265616D46756E631A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756522002801123F0A0964656C6574655F666E12122E44656C65746546756E63526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652200122F0A0773686F775F666E12102E53686F7746756E63526571756573741A102E46756E63566572526573706F6E7365220012380A0C73686F775F616C6C5F666E7312132E53686F77416C6C46756E63526571756573741A0F2E4164645265717565737446756E6322003001124C0A1673686F775F616C6C5F776974685F6372697465726961121B2E53686F77416C6C46756E634372697465726961526571756573741A0F2E4164645265717565737446756E63220028013001620670726F746F33", "google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33"};
}

