import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "gRPCServiceAllFunctions" on ep {

    remote function add_new_fn(AddRequestFunc value) returns string|error {
    }
    remote function delete_fn(DeleteFuncRequest value) returns string|error {
    }
    remote function show_fn(ShowFuncRequest value) returns FuncVerResponse|error {
    }
    remote function add_fns(stream<AddRequestStreamFunc, grpc:Error?> clientStream) returns string|error {
    }
    remote function show_all_fns(ShowAllFuncRequest value) returns stream<AddRequestFunc, error?>|error {
    }
    remote function show_all_with_criteria(stream<ShowAllFuncCriteriaRequest, grpc:Error?> clientStream) returns stream<AddRequestFunc, error?>|error {
    }
}

